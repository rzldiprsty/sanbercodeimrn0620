//No. 1 Looping-While

var deret = 0;

console.log('LOOPING PERTAMA')
while(deret < 20) {
    deret+=2;
    console.log(deret + ' - I love coding');
}
console.log('LOOPING KEDUA')
while(deret >= 2) {
    console.log(deret + ' - I will become a mobile developer');
    deret-=2;
}


//No. 2 Looping menggunakan for

for( var angka = 1; angka <= 20; angka++) {
    if(angka % 2 == 0) {
        console.log(angka + ' - Berkualitas');
    }
    else {
        if(angka % 3 == 0) {
            console.log(angka + ' - I Love Coding');
        }
        else {
            console.log(angka + ' - Santai');
        }
    }
}


//No. 3 Membuat Persegi Panjang #

for (var i=1; i<=4; i++) {
    console.log("########");
}


//No. 4 Membuat Tangga

var s = '';

for (var j=1; j<=7; j++) {
    for (var k=1; k<=j; k++) {
        s += '#';
    }
    s += '\n';
}
console.log(s);


//No. 5 Membuat Papan Catur

for (var l=1; l<=8; l++) { 
    if(l%2 == 0) {
        console.log('# # # # ');
    }else {
        console.log(" # # # #");
    }
}

