//No.1 (Range)

function range (startNum, finishNum){
    var array = [];
    var i;
    if ( startNum < finishNum ){
        for (i = startNum; i <= finishNum; i++) {
            array.push(i);
        }
    }
    else if ( startNum > finishNum ){
        for (i = startNum; i >= finishNum; i--) {
            array.push(i);
        }
    }
    else {
        return -1;
    }
    return array;
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());


//No.2 (Range with Step)

function rangeWithStep (otherstartNum, otherfinishNum, step){
    var otherArray = [];
    var j;
    if ( otherstartNum < otherfinishNum ){
        for (j = otherstartNum; j <= otherfinishNum; j += step){
            otherArray.push(j);
        }
    }
    else {
        for (j = otherstartNum; j >= otherfinishNum; j -= step){
            otherArray.push(j);
        }
    }
    return otherArray;
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));


//No.3 (Sum of Range)

function sum (start, end, otherStep){
    if ( otherStep === undefined ){
        otherStep = 1;
    }

    var newArray = [];
    var k;
    let result = 0;

    if ( start < end ){
        for (k = start; k <= end; k += otherStep){
            newArray.push(k);
        }
    }
    else if ( start > end ){
        for (k = start; k >= end; k -= otherStep){
            newArray.push(k);
        }
    }
    else {
        if ( end === undefined){
            if ( start === undefined){
                return 0;
            }
            else{
                return start;
            }
        }
    }

    for (k = 0; k < newArray.length; k++){
        result += newArray[k]; 
    }
    return result;
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());


//No.4 (Array Multidimensi)
 

    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]
    for ( let x = 0; x < input.length; x++){
        console.log('Nomor ID: ' + input[x][0]);
        console.log('Nama Lengkap: ' + input[x][1]);
        console.log('TTL: ' + input[x][2] + ' ' + input[x][3]);
        console.log('Hobi: ' + input[x][4]);
        console.log('');
    }


//No.5 (Balik Kata)

function balikKata(string){
    let mirror = "";
    var z;
    for ( z = string.length - 1; z >= 0; z--){
        mirror += string[z]
    }
    return mirror;
}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));


//No.6 (Metode Array)



    


    

