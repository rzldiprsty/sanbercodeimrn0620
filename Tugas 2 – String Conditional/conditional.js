//If-else

var nama = "John";
var peran = "Guard";

if ( nama == "John" ) {
    console.log("Halo John, Pilih peranmu untuk memulai game!");
}else if (nama == "Jane" || peran == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf, Jane");
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")   ; 
}else if ( nama == "Jenita" || peran == "Guard") {
    console.log("Selamat datang di Dunia Werewolf, Jaenita");
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
}else if ( nama == "Junaedi" || peran == "Warewolf") {
    console.log("Selamat datang di Dunia Werewolf, Junaedi");
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
}else {
    console.log("Nama harus diisi!");
}

//Switch Case

var tanggal = 11;
var bulan = 4;      //Type data inputnya Integer yaa
var tahun = 2000; 

if (tanggal >= 1 && tanggal <= 31) {
    if (bulan >= 1 && bulan <= 12) {
        if (tahun >= 1900 && tahun <= 2200) {

            switch(bulan) {
                case 1:   { console.log(tanggal + ' Januari ' + tahun); break; }
                case 2:   { console.log(tanggal + ' Februari ' + tahun); break; }
                case 3:   { console.log(tanggal + ' Maret ' + tahun); break; }
                case 4:   { console.log(tanggal + ' April ' + tahun); break; }
                case 5:   { console.log(tanggal + ' Mei ' + tahun); break; }
                case 6:   { console.log(tanggal + ' Juni ' + tahun); break; }
                case 7:   { console.log(tanggal + ' Juli ' + tahun); break; }
                case 8:   { console.log(tanggal + ' Agustus ' + tahun); break; }
                case 9:   { console.log(tanggal + ' September ' + tahun); break; }
                case 10:   { console.log(tanggal + ' Oktober ' + tahun); break; }
                case 11:   { console.log(tanggal + ' November ' + tahun); break; }
                case 12:   { console.log(tanggal + ' Desember ' + tahun); break; }
                default:   { console.log('Tidak terjadi apa-apa'); }
            }
        }
    }
}


