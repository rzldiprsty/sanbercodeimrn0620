/*
  A. Bandingkan Angka (10 poin)
    Buatlah sebuah function dengan nama bandingkan() yang menerima sebuah parameter berupa number 
    dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
    maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
    dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
    maka function akan mereturn nilai -1. 

  B. Balik String (10 poin)
    Diketahui sebuah function balikString yang menerima satu buah parameter berupa tipe data string. Function balikString akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. contoh: balikString("Javascript") akan me-return string "tpircsavaJ", balikString("satu") akan me-return string "utas", dst.

    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 

  C. Palindrome (10 poin)
    Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
    Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
    Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
    Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 

  
    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 
  
    
*/


//-----A. Bandingkan Angka-----
function bandingkan(num1, num2) {

  if (num2 === undefined){
    num2 = 0;
  }
  if ( num1 < 0){
    return -1;
  }
  if ( num2 < 0){
    return -1
  }
  else if ( num1 > num2){
    return num1;
  }
  else if ( num1 < num2){
    return num2;
  }
  else {
    if ( num1 = num2 ){
      return -1;
    }
    else{
      return -1;
    }
  }
}
// TEST CASES
console.log(bandingkan(10, 15));
console.log(bandingkan(12, 12));
console.log(bandingkan(-1, 10));
console.log(bandingkan(112, 121));
console.log(bandingkan(1));
console.log(bandingkan());
console.log(bandingkan("15", "18"));


//-----B. Balik String-----
function balikString(string) {
  let balikkata = "";
  for ( var i = string.length - 1; i >= 0; i--){
    balikkata += string[i];
  }
  return balikkata;
}
// TEST CASES
console.log(balikString("abcde"));
console.log(balikString("rusak"));
console.log(balikString("racecar"));
console.log(balikString("haji"));


//-----C. Palindrome-----
function palindrome(str) {
  var len = Math.floor(str.length / 2);
  for ( var i = 0; i < len; i++){
    if (str[i] !== str[str.length - i - 1]){
      return false;
    }
    else {
      return true;
    }
  }
}
// TEST CASES
console.log(palindrome("kasur rusak"))
console.log(palindrome("haji ijah"))
console.log(palindrome("nabasan"))
console.log(palindrome("nababan"))
console.log(palindrome("jakarta"))
